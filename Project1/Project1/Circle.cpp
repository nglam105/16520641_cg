#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int new_x;
	int new_y;

	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
	SDL_RenderDrawPoint(ren, xc + y, yc + x);
	SDL_RenderDrawPoint(ren, xc - y, yc + x);
	SDL_RenderDrawPoint(ren, xc + y, yc - x);
	SDL_RenderDrawPoint(ren, xc - y, yc - x);


	//7 points
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0, y = R;
	int p = 3 - 2 * R;
	Draw8Points(xc, yc, x, y,ren);
	while (y >= x)
	{
		
		x++;
		if (p > 0)
		{
			y--;
			p = p + 4 * (x - y) + 10;
		}
		else
			p = p + 4 * x + 6;
		Draw8Points(xc, yc, x, y, ren);
	}
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x, y,p;
	x = 0;
	y = R;
	p = 1 - R;
	Draw8Points(xc, yc, x, y, ren);
	while (x <= y)
	{
		if (p < 0)
			p += 2 * x + 3;
		else
		{
			p += 2 * (x - y) + 5;
			y--;

		}
		x++;
		Draw8Points(xc, yc, x, y, ren);
	}
}
