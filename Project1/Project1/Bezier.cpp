#include "Bezier.h"
#include<SDL.h>
#include <iostream>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	
			Vector2D v1;
			for (float i = 0; i <= 1; i = i + 0.0001)
			{
				v1.x = (1 - i)*(1 - i)*p1.x + 2 * (1 - i)*i*p2.x + i * i*p3.x;
				v1.y = (1 - i)*(1 - i)*p1.y + 2 * (1 - i)*i*p2.y + i * i*p3.y;
				SDL_RenderDrawPoint(ren, v1.x, v1.y);
			}
					

}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	Vector2D v1;
	for (float i = 0; i <= 1; i = i + 0.0001)
	{
		v1.x = (1 - i)*(1 - i)*(1 - i)*p1.x + 3 * (1 - i)*(1 - i)*i*p2.x + 3 * (1 - i)*i*i*p3.x + i * i*i*p4.x;
		v1.y = (1 - i)*(1 - i)*(1 - i)*p1.y + 3 * (1 - i)*(1 - i)*i*p2.y + 3 * (1 - i)*i*i*p3.y + i * i*i*p4.y;
		SDL_RenderDrawPoint(ren, v1.x, v1.y);
	}
}


