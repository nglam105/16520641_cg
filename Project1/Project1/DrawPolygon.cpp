#include "DrawPolygon.h"
#include <iostream>
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3], y[3];
	x[0] = 0;
	y[0] = -R;
	x[1] = (int)(cos(2 * M_PI / 3)*x[0] - y[0] * sin(2 * M_PI / 3) + 0.5);
	y[1] = (int)(x[0] * sin(2 * M_PI / 3) + y[0] * cos(2 * M_PI / 3) + 0.5);
	x[2] = (int)(cos(2 * M_PI / 3)*x[1] - y[1] * sin(2 * M_PI / 3) + 0.5);
	y[2] = (int)(x[1] * sin(2 * M_PI / 3) + y[1] * cos(2 * M_PI / 3) + 0.5);
	Bresenham_Line(x[0] + xc, y[0] + yc, x[1] + xc, y[1] + yc, ren);
	Bresenham_Line(x[1] + xc, y[1] + yc, x[2] + xc, y[2] + yc, ren);
	Bresenham_Line(x[0] + xc, y[0] + yc, x[2] + xc, y[2] + yc, ren);
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4];
	int y[4];
	float angle = M_PI / 4;
	for (int i = 0; i < 4; i++)
	{
		x[i] = int(-R * cos(angle) + 0.5);
		y[i] = int(-R * sin(angle) + 0.5);
		angle = angle + 2 * M_PI / 4;
	}
	Bresenham_Line(x[0] + xc, y[0] + yc, x[1] + xc, y[1] + yc, ren);
	Bresenham_Line(x[1] + xc, y[1] + yc, x[2] + xc, y[2] + yc, ren);
	Bresenham_Line(x[2] + xc, y[2] + yc, x[3] + xc, y[3] + yc, ren);
	Bresenham_Line(x[3] + xc, y[3] + yc, x[0] + xc, y[0] + yc, ren);
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5];
	int y[5];
	float angle = M_PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = int(-R * cos(angle) + 0.5);
		y[i] = int(-R * sin(angle) + 0.5);
		angle = angle + 2 * M_PI / 5;
	}

	Bresenham_Line(x[0] + xc, y[0] + yc, x[1] + xc, y[1] + yc, ren);
	Bresenham_Line(x[1] + xc, y[1] + yc, x[2] + xc, y[2] + yc, ren);
	Bresenham_Line(x[2] + xc, y[2] + yc, x[3] + xc, y[3] + yc, ren);
	Bresenham_Line(x[3] + xc, y[3] + yc, x[4] + xc, y[4] + yc, ren);
	Bresenham_Line(x[0] + xc, y[0] + yc, x[4] + xc, y[4] + yc, ren);
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6];
	int y[6];
	float angle = 0;
	for (int i = 0; i < 6; i++)
	{
		x[i] = int(-R * cos(angle) + 0.5);
		y[i] = int(-R * sin(angle) + 0.5);
		angle = angle + 2 * M_PI / 6;
	}

	Bresenham_Line(x[0] + xc, y[0] + yc, x[1] + xc, y[1] + yc, ren);
	Bresenham_Line(x[1] + xc, y[1] + yc, x[2] + xc, y[2] + yc, ren);
	Bresenham_Line(x[2] + xc, y[2] + yc, x[3] + xc, y[3] + yc, ren);
	Bresenham_Line(x[3] + xc, y[3] + yc, x[4] + xc, y[4] + yc, ren);
	Bresenham_Line(x[4] + xc, y[4] + yc, x[5] + xc, y[5] + yc, ren);
	Bresenham_Line(x[5] + xc, y[5] + yc, x[0] + xc, y[0] + yc, ren);
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5];
	int y[5];
	float angle = M_PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = int(-R * cos(angle) + 0.5);
		y[i] = int(-R * sin(angle) + 0.5);
		angle = angle + 2 * M_PI / 5;
	}
	Bresenham_Line(x[0] + xc, y[0] + yc, x[2] + xc, y[2] + yc, ren);
	Bresenham_Line(x[0] + xc, y[0] + yc, x[3] + xc, y[3] + yc, ren);
	Bresenham_Line(x[1] + xc, y[1] + yc, x[3] + xc, y[3] + yc, ren);
	Bresenham_Line(x[1] + xc, y[1] + yc, x[4] + xc, y[4] + yc, ren);
	Bresenham_Line(x[2] + xc, y[2] + yc, x[4] + xc, y[4] + yc, ren);
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], xx[5];
	int y[5], yy[5];
	float angle = M_PI / 2;
	float r = R * sin(M_PI / 10) / sin(7 * M_PI / 10);
	for (int i = 0; i < 5; i++)
	{
		x[i] = int(-R * cos(angle) + 0.5);
		y[i] = int(-R * sin(angle) + 0.5);
		xx[i] = int(r * cos(angle) + 0.5);
		yy[i] = int(r * sin(angle) + 0.5);
		angle += 2 * M_PI / 5;
	}
	Bresenham_Line(x[0] + xc, y[0] + yc, xx[3] + xc, yy[3] + yc, ren);
	Bresenham_Line(x[0] + xc, y[0] + yc, xx[2] + xc, yy[2] + yc, ren);

	Bresenham_Line(x[1] + xc, y[1] + yc, xx[3] + xc, yy[3] + yc, ren);
	Bresenham_Line(x[1] + xc, y[1] + yc, xx[4] + xc, yy[4] + yc, ren);

	Bresenham_Line(x[2] + xc, y[2] + yc, xx[0] + xc, yy[0] + yc, ren);
	Bresenham_Line(x[2] + xc, y[2] + yc, xx[4] + xc, yy[4] + yc, ren);

	Bresenham_Line(x[3] + xc, y[3] + yc, xx[0] + xc, yy[0] + yc, ren);
	Bresenham_Line(x[3] + xc, y[3] + yc, xx[1] + xc, yy[1] + yc, ren);

	Bresenham_Line(x[4] + xc, y[4] + yc, xx[1] + xc, yy[1] + yc, ren);
	Bresenham_Line(x[4] + xc, y[4] + yc, xx[2] + xc, yy[2] + yc, ren);


}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8], y[8];
	x[0] = 0;
	y[0] = -R;
	for (int i = 1; i < 8; i++)
	{
		x[i] = (int)(cos(2 * M_PI / 8)*x[i - 1] - y[i - 1] * sin(2 * M_PI / 8) + 0.5);
		y[i] = (int)(x[i - 1] * sin(2 * M_PI / 8) + y[i - 1] * cos(2 * M_PI / 8) + 0.5);
	}
	int xx[8], yy[8];
	int r = (R*sin(M_PI / 8)) / (sin(3 * M_PI / 4));
	int xx0 = 0;
	int yy0 = -r;
	xx[0] = (int)((cos(2 * M_PI / 16)*xx0 - yy0 * sin(2 * M_PI / 16) + 0.5));
	yy[0] = (int)((xx0 * sin(2 * M_PI / 16) + yy0 * cos(2 * M_PI / 16) + 0.5));
	for (int i = 1; i < 8; i++)
	{
		xx[i] = (int)(cos(2 * M_PI / 8)*xx[i - 1] - yy[i - 1] * sin(2 * M_PI / 8) + 0.5);
		yy[i] = (int)(xx[i - 1] * sin(2 * M_PI / 8) + yy[i - 1] * cos(2 * M_PI / 8) + 0.5);
	}
	Bresenham_Line(x[0] + xc, y[0] + yc, xx[7] + xc, yy[7] + yc, ren);
	Bresenham_Line(x[0] + xc, y[0] + yc, xx[0] + xc, yy[0] + yc, ren);

	Bresenham_Line(x[1] + xc, y[1] + yc, xx[0] + xc, yy[0] + yc, ren);
	Bresenham_Line(x[1] + xc, y[1] + yc, xx[1] + xc, yy[1] + yc, ren);

	Bresenham_Line(x[2] + xc, y[2] + yc, xx[1] + xc, yy[1] + yc, ren);
	Bresenham_Line(x[2] + xc, y[2] + yc, xx[2] + xc, yy[2] + yc, ren);

	Bresenham_Line(x[3] + xc, y[3] + yc, xx[2] + xc, yy[2] + yc, ren);
	Bresenham_Line(x[3] + xc, y[3] + yc, xx[3] + xc, yy[3] + yc, ren);

	Bresenham_Line(x[4] + xc, y[4] + yc, xx[3] + xc, yy[3] + yc, ren);
	Bresenham_Line(x[4] + xc, y[4] + yc, xx[4] + xc, yy[4] + yc, ren);

	Bresenham_Line(x[5] + xc, y[5] + yc, xx[4] + xc, yy[4] + yc, ren);
	Bresenham_Line(x[5] + xc, y[5] + yc, xx[5] + xc, yy[5] + yc, ren);

	Bresenham_Line(x[6] + xc, y[6] + yc, xx[5] + xc, yy[5] + yc, ren);
	Bresenham_Line(x[6] + xc, y[6] + yc, xx[6] + xc, yy[6] + yc, ren);

	Bresenham_Line(x[7] + xc, y[7] + yc, xx[6] + xc, yy[6] + yc, ren);
	Bresenham_Line(x[7] + xc, y[7] + yc, xx[7] + xc, yy[7] + yc, ren);
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int x[5], xx[5];
	int y[5], yy[5];
	float angle = startAngle;
	float r = R * sin(M_PI / 10) / sin(7 * M_PI / 10);
	for (int i = 0; i < 5; i++)
	{
		x[i] = int(-R * cos(angle) + 0.5);
		y[i] = int(-R * sin(angle) + 0.5);
		xx[i] = int(r * cos(angle) + 0.5);
		yy[i] = int(r * sin(angle) + 0.5);
		angle += 2 * M_PI / 5;
	}
	Bresenham_Line(x[0] + xc, y[0] + yc, xx[3] + xc, yy[3] + yc, ren);
	Bresenham_Line(x[0] + xc, y[0] + yc, xx[2] + xc, yy[2] + yc, ren);

	Bresenham_Line(x[1] + xc, y[1] + yc, xx[3] + xc, yy[3] + yc, ren);
	Bresenham_Line(x[1] + xc, y[1] + yc, xx[4] + xc, yy[4] + yc, ren);

	Bresenham_Line(x[2] + xc, y[2] + yc, xx[0] + xc, yy[0] + yc, ren);
	Bresenham_Line(x[2] + xc, y[2] + yc, xx[4] + xc, yy[4] + yc, ren);

	Bresenham_Line(x[3] + xc, y[3] + yc, xx[0] + xc, yy[0] + yc, ren);
	Bresenham_Line(x[3] + xc, y[3] + yc, xx[1] + xc, yy[1] + yc, ren);

	Bresenham_Line(x[4] + xc, y[4] + yc, xx[1] + xc, yy[1] + yc, ren);
	Bresenham_Line(x[4] + xc, y[4] + yc, xx[2] + xc, yy[2] + yc, ren);

}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	float angle = M_PI / 2;
	while (r > 0)
	{
		DrawStarAngle(xc, yc, r, angle, ren);
		r = sin(M_PI / 10)*r / sin(7 * M_PI / 10);
		angle = angle + M_PI;
	}
}
