#include "Ellipse.h"
#include<iostream>
void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	float p, a2, b2;
	int x, y;
	a2 = pow(a, 2);
	b2 = pow(b, 2);
	x = 0;
	y = b;
	p = -2*a2*b + a2 + 2*b2;
	// Area 1	
	Draw4Points(xc, yc, x, y, ren);
	while (((float)b2/a2)*x <= y)
	{	
		Draw4Points(xc, yc, x, y, ren);
		if (p <= 0)
		{
			p = p + 4 * b2*x + 6 * b2;
		}
		else
		{
			p = p + 4 * b2*x - 4 * a2*y + 6*b2;
			y--;

		}
		x++;
		
	}
	// Area 2
	y = 0;
	x = a;
	p = 2 * a2 - 2 * a*b2 + b2;
	while (((float)a2 / b2)*y <= x)
	{	Draw4Points(xc, yc, x, y, ren);
		if (p < 0)
		{
			p = p + 4 * a2*y + 6*a2;
		}
		else
		{
			p = p + 4 * a2*y + 6 * a2 - 4 * x*b2;
			x--;

		}
		y++;
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int x, y, fx, fy, a2, b2, p;
	x = 0;
	y = b;
	a2 = a * a;
	b2 = b * b;
	fx = 2 * b2 * x;
	fy = 2 * a2 * y;
	Draw4Points(xc, yc, a, 0, ren);
	p =b2 - (a2*b) + (0.25*a2);
	// Area 1
	while (fx<fy)
	{
		Draw4Points(xc, yc, x, y, ren);
		x++;
		fx += 2 * b2;
		if (p<0)
		{
			p += fx+b2;
		}
		else
		{
			y--;
			p += fx-fy+b2;
			fy -= 2 * a2;
		}
		
	}
	// Area 2
	while (y>0)
	{
		Draw4Points(xc, yc, x, y, ren);
		y--;
		fy -= 2 * a2;
		if (p >= 0)
		{
			p += a2-fy;
		}
		else
		{
			x++;
			fx += 2 * b2;
			p += fx-fy+a2;
		}
		
	}
}
